import {
  INPUT_CURSOR,
  INPUT_CUSTOM_EVENT,
  INPUT_METADATA, INPUT_SENSOR_ACCELEROMETER,
  INPUT_SENSOR_GYROSCOPE, INPUT_SENSOR_LINEAR_ACCELERATION,
  INPUT_SENSOR_MAGNETIC_FIELD, INPUT_TOUCH, INPUT_WHEEL,
} from './inputLoggers/input-names';

/**
 * Returns the default logger configuration.
 * @returns {!Object}
 */
export function getDefaultConfig() {
  return {
    apiUrl: null,
    inputs: [
      INPUT_CURSOR,
      INPUT_WHEEL,
      INPUT_TOUCH,
      INPUT_SENSOR_ACCELEROMETER,
      INPUT_SENSOR_GYROSCOPE,
      INPUT_METADATA,
      INPUT_CUSTOM_EVENT,
    ],
    batchSize: 100,
    batchIntervalMilliseconds: 500,
    logToConsole: false,
    sensorConfig: {
      [INPUT_SENSOR_LINEAR_ACCELERATION]: {
        frequency: 60,
      },
      [INPUT_SENSOR_ACCELEROMETER]: {
        frequency: 60,
      },
      [INPUT_SENSOR_GYROSCOPE]: {
        frequency: 60,
      },
      [INPUT_SENSOR_MAGNETIC_FIELD]: {
        frequency: 10,
      },
    },
    dataEndpointUrl: 'data',
    sessionEndpointUrl: 'session',
    postLoggerHeader: 'behametrics/web',
  };
}

/**
 * Returns a logger configuration object whose missing entries are filled with values from `defaultConfig`. If `null`,
 * the entire contents of `defaultConfig` is used.
 * @param {?Object} config Logger configuration.
 * @param {!Object} defaultConfig Default logger configuration.
 */
export function getFilledConfigWithDefaults(config, defaultConfig = getDefaultConfig()) {
  let newConfig;

  if (config != null) {
    newConfig = {...config};
  } else {
    newConfig = {};
  }

  const compositeEntries = ['inputs', 'sensorConfig'];

  Object.keys(defaultConfig).forEach((key) => {
    if (compositeEntries.includes(key)) {
      return;
    }

    if (newConfig[key] == null) {
      newConfig[key] = defaultConfig[key];
    }
  });

  if (defaultConfig.inputs != null && newConfig.inputs == null) {
    newConfig.inputs = [...defaultConfig.inputs];
  }

  if (defaultConfig.sensorConfig != null) {
    if (newConfig.sensorConfig == null) {
      newConfig.sensorConfig = {};
    }

    Object.keys(defaultConfig.sensorConfig).forEach((key) => {
      newConfig.sensorConfig[key] = {
        ...defaultConfig.sensorConfig[key],
        ...newConfig.sensorConfig[key],
      };
    });
  }

  return newConfig;
}
