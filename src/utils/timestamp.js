const NANOSECONDS_PER_MILLISECOND = 1000000;

/**
 * Returns a timestamp in nanoseconds since the beginning of the epoch.
 * @param {number} elapsedTimeMilliseconds Time elapsed since `documentStartTimestampMilliseconds`.
 * @param {number} documentStartTimestampMilliseconds Timestamp of the start of the current document's lifetime since
 *     the beginning of the epoch.
 */
export function getTimestampNanoseconds(elapsedTimeMilliseconds, documentStartTimestampMilliseconds) {
  return (elapsedTimeMilliseconds + documentStartTimestampMilliseconds) * NANOSECONDS_PER_MILLISECOND;
}

/**
 * Returns the time in milliseconds elapsed since the start of the current document's lifetime or, if not available,
 * since the call to this function.
 */
export function getDocumentStartTimestamp() {
  let documentStartTimestamp = +new Date();

  if (window.performance != null && window.performance.timeOrigin != null) {
    documentStartTimestamp = window.performance.timeOrigin;
  }

  return documentStartTimestamp;
}
