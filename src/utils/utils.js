/**
 * Returns true if current device is a mobile device, false otherwise.
 */
export function isMobileDevice() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

/**
 * Returns a string representing the type of the current device.
 */
export function getDeviceType() {
  if (isMobileDevice()) {
    return 'mobile';
  } else {
    return 'desktop';
  }
}

/**
 * Returns true if the specified argument is of type {@link Object}, false otherwise.
 * @param {*} arg Argument to check.
 */
export function isObject(arg) {
  return arg === Object(arg);
}
