import {CursorLogger} from './cursor-logger';
import {WheelLogger} from './wheel-logger';
import {KeyboardLogger} from './keyboard-logger';
import {TouchLogger} from './touch-logger';
import {AccelerometerLogger, GyroscopeLogger, LinearAccelerationLogger, MagneticFieldLogger} from './sensor-loggers';
import {MetadataLogger} from './metadata-logger';
import {CustomEventLogger} from './custom-event-logger';
import {
  INPUT_CURSOR,
  INPUT_CUSTOM_EVENT,
  INPUT_KEYBOARD,
  INPUT_METADATA,
  INPUT_SENSOR_ACCELEROMETER,
  INPUT_SENSOR_GYROSCOPE,
  INPUT_SENSOR_LINEAR_ACCELERATION,
  INPUT_SENSOR_MAGNETIC_FIELD,
  INPUT_TOUCH,
  INPUT_WHEEL,
} from './input-names';

/**
 * Returns a map of input names to objects holding a subtype of {@link InputLogger} and the type(s) of devices the
 * logger is applicable for - `'mobile'` for mobile devices, `'desktop'` for desktop computers and laptops.
 * @returns {Map<String, Object>}
 */
export function getInputNamesAndLoggers() {
  return new Map([
    [INPUT_CURSOR, {
      LoggerType: CursorLogger,
      deviceTypes: ['desktop'],
    }],
    [INPUT_WHEEL, {
      LoggerType: WheelLogger,
      deviceTypes: ['desktop'],
    }],
    [INPUT_KEYBOARD, {
      LoggerType: KeyboardLogger,
      deviceTypes: ['desktop'],
    }],
    [INPUT_TOUCH, {
      LoggerType: TouchLogger,
      deviceTypes: ['mobile'],
    }],
    [INPUT_SENSOR_LINEAR_ACCELERATION, {
      LoggerType: LinearAccelerationLogger,
      deviceTypes: ['mobile'],
    }],
    [INPUT_SENSOR_ACCELEROMETER, {
      LoggerType: AccelerometerLogger,
      deviceTypes: ['mobile'],
    }],
    [INPUT_SENSOR_GYROSCOPE, {
      LoggerType: GyroscopeLogger,
      deviceTypes: ['mobile'],
    }],
    [INPUT_SENSOR_MAGNETIC_FIELD, {
      LoggerType: MagneticFieldLogger,
      deviceTypes: ['mobile'],
    }],
    [INPUT_METADATA, {
      LoggerType: MetadataLogger,
      deviceTypes: ['desktop', 'mobile'],
    }],
    [INPUT_CUSTOM_EVENT, {
      LoggerType: CustomEventLogger,
      deviceTypes: ['desktop', 'mobile'],
    }],
  ]);
}
