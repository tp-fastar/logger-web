#!/bin/bash

set -e


# Taken from: https://stackoverflow.com/a/246128
SCRIPT_DIRPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"
SCRIPT_NAME="$(basename -- "$0")"

REPOSITORY_DIRPATH="$(dirname "$SCRIPT_DIRPATH")"
MAIN_DEV_BRANCH='master'

PROJECT_ID=8780979
NUM_APPROVALS_BEFORE_MERGE=1


error_echo_and_exit() {
  echo "$1" 1>&2
  exit "${2:-1}"
}

submit_merge_request() {
  local title="$1"
  local tag="$2"
  local release_branch="$3"
  local gitlab_token="$4"

  git tag "$tag"
  git push origin "$release_branch" --tags
  git checkout master

  curl --data "source_branch=${release_branch}&target_branch=release&title=${title}&remove_source_branch=true&allow_collaboration=true&approvals_before_merge=${NUM_APPROVALS_BEFORE_MERGE}" --header "PRIVATE-TOKEN: $gitlab_token" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests"
  curl --data "source_branch=${release_branch}&target_branch=master&title=${title}&remove_source_branch=true&allow_collaboration=true&approvals_before_merge=${NUM_APPROVALS_BEFORE_MERGE}" --header "PRIVATE-TOKEN: $gitlab_token" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests"
}


if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  error_echo_and_exit "Usage: $SCRIPT_NAME <version> <gitlab-token>"
fi

current_branch="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$current_branch" != "$MAIN_DEV_BRANCH" ]]; then
  error_echo_and_exit "Wrong branch, switch to '$MAIN_DEV_BRANCH' to proceed"
fi

if [[ "$(git status --porcelain)" ]]; then
  error_echo_and_exit "Branch '$MAIN_DEV_BRANCH' is not clean, please commit or discard any working changes"
fi


npm run build

tag="$(npm version --no-git-tag-version "v$1")"
version="$1"

title="Release $version"
release_branch="Release-${version}"

git checkout -b "$release_branch"

git add "${REPOSITORY_DIRPATH}/package.json"
git add "${REPOSITORY_DIRPATH}/package-lock.json"
git commit -m "$title"

submit_merge_request "$title" "$tag" "$release_branch" "$2"
