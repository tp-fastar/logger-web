import chai from 'chai';
import {getDefaultConfig} from '../../src/config';
import {TouchLogger} from '../../src/inputLoggers/touch-logger';
import {INPUT_TOUCH} from '../../src/inputLoggers/input-names';
import {testEvents} from './touch-logger-test-suite';

const expect = chai.expect;
let logger;

describe('Given an instance of TouchLogger', () => {
  before(() => {
    logger = new TouchLogger(getDefaultConfig());
  });

  describe('When a TouchLogger instance is created', () => {
    it('the instance should be defined', () => {
      expect(logger).not.to.be.undefined;
    });
  });

  testEvents.forEach((eventData) => {
    describe('When touch events are obtained', () => {
      it('the events should contain expected data', () => {
        const touchesCommonFields = {
          screenX: 5,
          screenY: 5,
          force: 0.2,
        };

        eventData.touches.forEach((element, index) => {
          eventData.touches[index] = {...touchesCommonFields, ...element};
        });

        const event = {
          type: eventData.type,
          timeStamp: 456,
          changedTouches: eventData.touches,
        };

        const expectedResult = [];

        eventData.expectedEventsContent.forEach((element) => {
          expectedResult.push({
            input: INPUT_TOUCH,
            timestamp: 456,
            content: {
              x: 5,
              y: 5,
              pressure: 0.2,
              ...element,
            },
          });
        });

        expect(logger.getTouchEvents(event)).to.deep.equal(expectedResult);
      });
    });
  });
});
