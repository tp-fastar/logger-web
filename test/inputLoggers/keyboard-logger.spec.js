import chai from 'chai';
import {getDefaultConfig} from '../../src/config';
import {KeyboardLogger} from '../../src/inputLoggers/keyboard-logger';
import {INPUT_KEYBOARD} from '../../src/inputLoggers/input-names';
import {testEvents} from './keyboard-logger-test-suite';

const expect = chai.expect;
let logger;

describe('Given an instance of KeyboardLogger', () => {
  before(() => {
    logger = new KeyboardLogger(getDefaultConfig());
  });

  describe('When a KeyboardLogger instance is created', () => {
    it('the instance should be defined', () => {
      expect(logger).not.to.be.undefined;
    });
  });

  testEvents.forEach((eventData) => {
    describe('When keyboard events are obtained', () => {
      it('the events should contain expected data', () => {
        const event = {
          timeStamp: 456,
          ...eventData.event,
        };

        const expectedResult = {
          input: INPUT_KEYBOARD,
          timestamp: 456,
          content: eventData.expectedContent,
        };

        expect(logger.getKeyboardEvent(event)).to.deep.equal(expectedResult);
      });
    });
  });
});
