import chai from 'chai';
import {getDefaultConfig} from '../../src/config';
import {testEvents} from './sensor-loggers-test-suite';

const expect = chai.expect;

describe('Given an instance of AccelerometerLogger', () => {
  testEvents.forEach((eventData) => {
    const logger = new eventData.SensorLoggerType(getDefaultConfig());

    describe(`When a sensor event using the Generic Sensor API is obtained`, () => {
      it(`the event should contain expected data`, () => {
        const event = {
          timeStamp: 456,
        };

        const expectedResult = {
          timestamp: 456,
          ...eventData.expectedData,
        };

        expect(logger.getSensorEventWithGenericApi(eventData.genericApiData, event)).to.deep.equal(expectedResult);
      });
    });

    if (eventData.deviceMotionData != null) {
      describe(`When a sensor event using the DeviceMotionEvent is obtained`, () => {
        it(`the event should contain expected data`, () => {
          const expectedResult = {
            timestamp: 456,
            ...eventData.expectedData,
          };

          const event = {
            timeStamp: 456,
            ...eventData.deviceMotionData,
          };

          expect(logger.getSensorEventWithDeviceMotion(event)).to.deep.equal(expectedResult);
        });
      });
    }
  });
});
