import chai from 'chai';
import {getDefaultConfig} from '../../src/config';
import {CursorLogger} from '../../src/inputLoggers/cursor-logger';
import {INPUT_CURSOR} from '../../src/inputLoggers/input-names';
import {testEvents} from './cursor-logger-test-suite';

const expect = chai.expect;
let logger;

describe('Given an instance of CursorLogger', () => {
  before(() => {
    logger = new CursorLogger(getDefaultConfig());
  });

  describe('When a CursorLogger instance is created', () => {
    it('the instance should be defined', () => {
      expect(logger).not.to.be.undefined;
    });
  });

  testEvents.forEach((eventData) => {
    describe('When cursor events are obtained', () => {
      it('the events should contain expected data', () => {
        const event = {
          timeStamp: 456,
          screenX: 5,
          screenY: 5,
          ...eventData.event,
        };

        const expectedResult = [];

        eventData.expectedEventsContent.forEach((element) => {
          expectedResult.push({
            input: INPUT_CURSOR,
            timestamp: 456,
            content: {
              x: 5,
              y: 5,
              ...element,
            },
          });
        });

        expect(logger.getCursorEvents(event)).to.deep.equal(expectedResult);
      });
    });
  });
});
