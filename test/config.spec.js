import chai from 'chai';
import {testData} from './config-test-suite';
import {getFilledConfigWithDefaults} from '../src/config';

const expect = chai.expect;

describe('getFilledConfigWithDefaults()', () => {
  testData.forEach((configData) => {
    it('should fill missing entries with default values', () => {
      const result = getFilledConfigWithDefaults(configData.config, configData.defaultConfig);
      expect(result).to.deep.equal(configData.expectedConfig);
    });
  });

  it('should create copies of array config entries', () => {
    const defaultConfig = {
      inputs: ['cursor', 'wheel'],
    };

    const filledConfig = getFilledConfigWithDefaults({}, defaultConfig);

    filledConfig.inputs.push('touch');

    expect(filledConfig.inputs).to.deep.equal(['cursor', 'wheel', 'touch']);
    expect(defaultConfig.inputs).to.deep.equal(['cursor', 'wheel']);
  });
});
