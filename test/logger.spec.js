import chai from 'chai';
import sinon from 'sinon';
import {Logger} from '../src';
import {testStartAndStopData} from './logger-test-suite';
import * as utilsModule from '../src/utils/utils';

const expect = chai.expect;
let logger;
let stubGetDeviceType;

describe('Given an instance of Logger', () => {
  beforeEach(() => {
    logger = new Logger({apiUrl: 'http://127.0.0.1:5000'});
    logger.sendLoggedEvents_ = sinon.stub();

    logger.inputNamesAndLoggers_.forEach((loggerData) => {
      loggerData.logger.start = sinon.spy();
      loggerData.logger.stop = sinon.spy();
    });

    stubGetDeviceType = sinon.stub(utilsModule, 'getDeviceType');
  });

  afterEach(() => {
    stubGetDeviceType.restore();
  });

  describe('When a Logger instance is created', () => {
    it('the instance should be defined', () => {
      expect(logger).not.to.be.undefined;
    });

    it('the instance should have `documentStartTimestamp` equal to a valid number', () => {
      expect(+logger.documentStartTimestamp_).to.not.be.NaN;
    });
  });

  testStartAndStopData.forEach((item) => {
    describe(`When logging is started and then stopped with device type '${item.deviceType}'`
             + ` and with inputs '${item.inputs}'`, () => {
      it(`only inputs '${item.expectedRunningInputs}' should be running`, () => {
        stubGetDeviceType.returns(item.deviceType);
        logger.config.inputs = item.inputs;

        logger.start();

        logger.inputNamesAndLoggers_.forEach((loggerData, inputName) => {
          if (item.expectedRunningInputs.includes(inputName)) {
            expect(loggerData.logger.start.calledOnce).to.be.true;
          } else {
            expect(loggerData.logger.start.calledOnce).to.be.false;
          }
        });

        logger.stop();

        logger.inputNamesAndLoggers_.forEach((loggerData, inputName) => {
          expect(loggerData.logger.stop.calledOnce).to.be.true;
        });
      });
    });
  });
});
