import chai from 'chai';
import {getTimestampNanoseconds} from '../../src/utils/timestamp';

const expect = chai.expect;

describe('getTimestampNanoseconds()', () => {
  it('should return sum of its parameters if they are all numbers', () => {
    expect(getTimestampNanoseconds(100, 456)).to.equal(556000000);
  });
});
