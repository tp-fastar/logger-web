# Behametrics - Web Logger

This is a JavaScript library that allows collecting (logging) events from pointing devices (mouse, touchpad) such as movement and clicks,
events from keyboards (key presses and releases) and events from mobile devices (touch screen, accelerometer, gyroscope, magnetometer).
For easy and fast integration, we recommend using the [Behametrics server](https://gitlab.com/behametrics/server) as a backend server.

## Installation

### Using a JavaScript package manager

If you use the `npm` package manager, fetch a local copy of the logger by running:
```
npm install @behametrics/logger-web
```

### Using the Downloaded Archive

**Download [Behametrics - Web Logger](https://gitlab.com/behametrics/logger-web/-/jobs/artifacts/release/download?job=npm-build)**

The downloaded archive contains the following files:
* `behametrics-logger.min.js` - This is the minified JavaScript library.
* `behametrics-logger.min.js.map` - This file maps the minified file to the original source code.
* `behametrics-logger.js` - This is the readable, unminified JavaScript library.
* `behametrics-logger.js.map` - This file maps the unminified file to the original source code.

Unzip the downloaded archive to your website's directory and add this to the head of your HTML code:
```HTML
<script src="/path/to/behametrics-logger.js"></script>
```

## Usage

First, create a logger instance with a configuration object:
```javascript
import {Logger} from "@behametrics/logger-web";

//...

let logger = new Logger({
    apiUrl: 'https://your-domain.com'
})
```

In Node.js, you may alternatively import the main module using `require`:
```javascript
var behametricsLogger = require("@behametrics/logger-web");

//...

let logger = new behametricsLogger.Logger({
    apiUrl: 'https://your-domain.com'
})
```

Creating a logger directly in HTML:
```HTML
<script>
  let logger = new behametricsLogger.Logger({
      apiUrl: 'https://your-domain.com'
  });
</script>
```

The `apiUrl` entry is a base endpoint URL.
This endpoint is used to send the logged data and receive a session ID (explained later).

You may configure many more properties in the configuration object such as the inputs to be logged (mouse cursor, touch, accelerometer, etc.).
For entries omitted from the configuration object, default values are used.
Check the [logger configuration](https://gitlab.com/behametrics/logger-web/wikis/Home#configuring-logger) for allowed entries and their default values.

Before starting the logger, initialize a new session:
```javascript
logger.init();
```

The logger obtains a unique session ID from the endpoint specified via the `apiUrl` configuration entry.
The session ID is associated with each logged event and identifies each logged event as belonging to this session.

To start logging, call `start()`:
```javascript
logger.start();
```

You may also start logging immediately after initializing a new session:
```javascript
logger.init().then(
    () => logger.start()
);
```

To stop logging data, simply call `stop()`:
```javascript
logger.stop();
```

### Data format

The following is an example of an event yielded by a pointing device such as a mouse, in JSON:
```json
{
  "input": "cursor",
  "session_id": "5c70579af2307d000b7eaf0c",
  "timestamp": 1542730372913000000,
  "content": [
    {
      "event_type": "move",
      "button": "",
      "x": 1123,
      "y": 547
    }
  ]
}
```

For detailed information about the format and contents of logged events, consult the
[specification](https://gitlab.com/behametrics/specification/blob/release/Specification.md#input-events).
For inputs supported by this logger, see the [logger configuration](https://gitlab.com/behametrics/logger-web/wikis/Home#configuring-logger).

### Logging Custom events

If you need to log custom data (e.g. clicking on a specific element or a user logging in), call `logCustomEvent()`:
```javascript
logger.logCustomEvent({
    action: 'login',
    name: 'John',
})
```

The input name may also be specified (if omitted, the name defaults to `'custom'`):
```javascript
logger.logCustomEvent({
    action: 'login',
    name: 'John',
},
'login')
```

Note that custom events will only be logged if the logger is running (i.e. `Logger.start()` was called).

### Logging Device Metadata

You may also log metadata associated with the device by calling `logger.logMetadata()`.
The metadata event contains properties of `window.navigator` and a few other entries.

Note that metadata will only be logged if the logger is running (i.e. `Logger.start()` was called).

## Documentation

For more information about the logger library, see:
* [wiki](https://gitlab.com/behametrics/logger-web/wikis),
* [API documentation](https://behametrics.gitlab.io/logger-web/doc/).

## License
Source code is provided under the [MIT License](LICENSE).
